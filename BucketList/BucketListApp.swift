//
//  BucketListApp.swift
//  BucketList
//
//  Created by Admin on 15.03.2022.
//

import SwiftUI

@main
struct BucketListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
