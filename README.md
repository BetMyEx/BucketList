# BucketList
This is an app that lets the user build a private list of places on the map that they intend to visit one day, add a description for that place, look up interesting places that are nearby, and save it all to the iOS storage for later.
## Used features
- using MapKit
- Using Touch ID and Face ID with SwiftUI
- Downloading data from Wikipedia
## App's overview
![screen-gif](./BucketList/BucketList.gif)
